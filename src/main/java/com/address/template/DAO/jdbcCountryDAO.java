package com.address.template.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.address.template.bean.Countries;
import com.address.template.bean.Countries2;
import com.address.template.bean.CountriesXML;

//@Component("jdbcCountryDAO")
public class jdbcCountryDAO {
	private final NamedParameterJdbcTemplate jdbcTemplate;
	
	
    @Autowired
    public jdbcCountryDAO(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
    
    public List<Countries> getCountryByID(int id) {
		int template_id = this.getCountryTemplateID(id);
		
    	String sql = "select * from template_fields where id_template = :id";// + template_id;
    	//System.out.println(template_id);

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", template_id);

		//return jdbcTemplate.queryForObject(sql, params, new CountriesRowMapper());
		//List<Countries> tmp = jdbcTemplate.query(sql, params, new CountriesRowMapper());
		
		return jdbcTemplate.query(sql, params, new CountriesRowMapper());
	}
    
    public Countries2 getCountryByID2(int id) {
		int template_id = this.getCountryTemplateID(id);
		
    	String sql = "select * from template_fields2 where id_template = :id";// + template_id;
    	System.out.println(template_id);

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", template_id);

		return jdbcTemplate.queryForObject(sql, params, new CountriesRowMapper2());
		
	}
    
    public CountriesXML getCountryByID3(int id) {
		int template_id = this.getCountryTemplateID(id);
		String sql = "select * from template_fields2 where id_template = :id";// + template_id;

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", template_id);

		return jdbcTemplate.queryForObject(sql, params, new CountriesRowMapper3());
		
	}
    
    public int getCountryTemplateID(int id) {
		String sql = "select template from countries where id = " + id;
		return jdbcTemplate.getJdbcOperations().queryForObject(sql, Integer.class);
	}
    
    private static final class CountriesRowMapper implements RowMapper<Countries> {

		@Override
		public Countries mapRow(ResultSet rs, int rowNum) throws SQLException {

			Countries country = new Countries();
			country.setField(rs.getString(2));

			return country;
		}

	}
    
    private static final class CountriesRowMapper2 implements RowMapper<Countries2> {

		@Override
		public Countries2 mapRow(ResultSet rs, int rowNum) throws SQLException {

			Countries2 country = new Countries2();
			country.setField1(rs.getString(3));
			country.setField2(rs.getString(4));
			country.setField3(rs.getString(5));
			country.setField4(rs.getString(6));
			country.setField5(rs.getString(7));
			country.setField6(rs.getString(8));
			country.setField7(rs.getString(9));
			country.setField8(rs.getString(10));
			country.setField9(rs.getString(11));
			country.setField10(rs.getString(12));
			country.setField11(rs.getString(13));
			country.setField12(rs.getString(14));
			country.setField13(rs.getString(15));
			country.setField14(rs.getString(16));
			country.setField15(rs.getString(17));
			
			country.setStatusCode("400");
			country.setStatusMessage("OK");
			
			return country;
		}

	}
    
    private static final class CountriesRowMapper3 implements RowMapper<CountriesXML> {

		@Override
		public CountriesXML mapRow(ResultSet rs, int rowNum) throws SQLException {

			CountriesXML country = new CountriesXML();
			country.setField1(rs.getString(3));
			country.setField2(rs.getString(4));
			country.setField3(rs.getString(5));
			country.setField4(rs.getString(6));
			country.setField5(rs.getString(7));
			country.setField6(rs.getString(8));
			country.setField7(rs.getString(9));
			country.setField8(rs.getString(10));
			country.setField9(rs.getString(11));
			country.setField10(rs.getString(12));
			country.setField11(rs.getString(13));
			country.setField12(rs.getString(14));
			country.setField13(rs.getString(15));
			country.setField14(rs.getString(16));
			country.setField15(rs.getString(17));
			
			country.setStatusCode("400");
			country.setStatusMessage("OK");
			
			return country;
		}

	}

}
