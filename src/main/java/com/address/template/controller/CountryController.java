package com.address.template.controller;

import java.util.List;
import javax.sql.DataSource;

import com.address.template.DAO.jdbcCountryDAO;
import com.address.template.bean.Countries;
import com.address.template.bean.Countries2;
import com.address.template.bean.CountriesXML;
import com.address.template.exceptions.CustomGenericException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CountryController {
	ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
	DataSource obj = (DataSource) context.getBean("dataSource");
	 
	 @RequestMapping(value = "/country/{id}", method = RequestMethod.GET, headers="Accept=application/json")
	 public List<Countries> getCountryById(@PathVariable int id)
	 {
		 jdbcCountryDAO countryDAO = new jdbcCountryDAO(obj);
		 List<Countries> country = countryDAO.getCountryByID(id);
		 
		 return country;
	
	 }
	 
	 @RequestMapping(value = "/country2/{id}", method = RequestMethod.GET, headers="Accept=application/json")
	 public Countries2 getCountryById2(@PathVariable int id)
	 {
		 jdbcCountryDAO countryDAO = new jdbcCountryDAO(obj);
		 Countries2 country = countryDAO.getCountryByID2(id);
		 
		 return country;
	
	 }
	 
	 
	 @RequestMapping(value = "/countryxml/{id}")
	 public CountriesXML getCountryById3(@PathVariable int id)
	 {
		 jdbcCountryDAO countryDAO = new jdbcCountryDAO(obj);
		 CountriesXML country = countryDAO.getCountryByID3(id);
		 
		 return country;
		 
	 }
	 
	 @ExceptionHandler(Exception.class)
	 public CustomGenericException handleAllException(Exception ex) {
		CustomGenericException err = new CustomGenericException();
	 	err.setStatusCode("404");
	 	err.setStatusMessage(ex.toString());
	
		return err;
	
	 }
 
}

